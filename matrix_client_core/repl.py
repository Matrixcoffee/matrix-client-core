# stdlib
import difflib
import pprint

# in-tree deps
from .exception import wrap_exception

class ConsoleClientMixin:
	def __init__(self):
		self.restrict_to_foreground = False
		self.foreground_room = None

	@staticmethod
	def _prettyprint_raw_event(prefix, event):
		print(prefix, end=' ')
		pprint.pprint(event)

	def _print_diff(self, prefix, event):
		self._print_diff_root(prefix, event)
		self._print_diff_unsigned(prefix, event)
		self._print_diff_checkunsigned(prefix, event)

	def _print_diff_root(self, prefix, event):
		try:
			a, b = event['prev_content'], event['content']
		except KeyError:
			return
		self._print_actual_diff(prefix + " Diff root/content:", a, b)

	def _print_diff_unsigned(self, prefix, event):
		try:
			a, b = event['unsigned']['prev_content'], event['content']
		except KeyError:
			return
		self._print_actual_diff(prefix + " Diff unsigned/content:", a, b)

	def _print_diff_checkunsigned(self, prefix, event):
		try:
			a, b = event['prev_content'], event['unsigned']['prev_content']
		except KeyError:
			return
		self._print_actual_diff(prefix + " Diff root/unsigned(!):", a, b)

	@staticmethod
	def _print_actual_diff(prefix, a, b):
		ppa = (pprint.pformat(a) + "\n").splitlines(True)
		ppb = (pprint.pformat(b) + "\n").splitlines(True)
		print(prefix)
		print("".join(difflib.ndiff(ppa, ppb)), end='')

	@wrap_exception
	def on_global_timeline_event(self, event):
		self.exceptionhandler.last_event = event
		roomid = event['room_id']
		roomhandle = self.rooms.get_room_handle(roomid)
		roomprefix = "[{0}]".format(roomhandle)

		if not self.restrict_to_foreground or (self.foreground_room and self.foreground_room.room_id == roomid):
			sender = event['sender']
			rich_sender = sender
			try:
				rich_sender = "{} ({})".format(sender, event['content']['displayname'])
			except KeyError:
				try:
					rich_sender = "{} ({})".format(sender, event['unsigned']['prev_content']['displayname'])
				except KeyError:
					pass

			if event['type'] == "m.room.member":
				if event['content']['membership'] == "join":
					print(roomprefix, "{} joined".format(rich_sender))
				elif event['content']['membership'] == "leave":
					print(roomprefix, "{} left".format(rich_sender))
				else:
					self._prettyprint_raw_event(roomprefix, event)
			elif event['type'] == "m.room.message":
				if event['content']['msgtype'] == "m.text":
					inmsg = event['content']['body']
					print(roomprefix, "{}: {}".format(rich_sender, inmsg))
				elif event['content']['msgtype'] == "m.emote":
					print(roomprefix, " * {} {}".format(rich_sender, event['content']['body']))
				else:
					print(roomprefix, " ? {}:{}: {}".format(
						rich_sender,
						event['content']['msgtype'],
						event['content'].get('body', "")))
			else:
				self._prettyprint_raw_event(roomprefix, event)

			self._print_diff(roomprefix, event)

		self.exceptionhandler.reset_exc_delay()

	def repl_debug(self, txt):
		""" Show more information about the last error that happened """
		info = self.exceptionhandler.debug_info
		if info is None:	print("No errors.")
		else:			print(info, end='')
		return True

	def repl_exception(self, txt):
		""" Raise an exception. For debugging. """
		raise Exception("This exception intended for debugging purposes only.")

	def repl_me(self, txt):
		""" Send an action/emote """
		if not self.foreground_room:
			print("Cannot send message: You have not selected any room. Try /help.")
			return True

		self.foreground_room.send_emote(txt[4:])
		return True

	def repl_list(self, txt):
		""" List the rooms you're a member of """

		print(" ".join(map(self.rooms.get_room_handle, self.rooms.roomsbyid.keys())))
		return True

	def repl_open(self, txt):
		""" Open a room you're already a member of """
		try:
			cmd, handle = txt.split(None, 1)
		except ValueError:
			print("Wrong number of arguments")
			return True

		room = self.rooms.get_room(handle)
		if room is None:
			print("You are not a member of that room. Did you want /join?")
			return True

		print("Opening room %s: %s" % (handle, room.display_name))
		print("Topic:", room.topic)
		self.foreground_room = room

		return True

	def repl_join(self, txt):
		""" Join a room you're not already a member of """
		try:
			cmd, roomid = txt.split(None, 1)
		except ValueError:
			print("Wrong number of arguments")
			return True

		self.foreground_room = self.sdkclient.join_room(roomid)
		print("Opening room %s: %s" % (roomid, self.foreground_room.display_name))
		print("Topic:", self.foreground_room.topic)
		return True

	def repl_part(self, txt):
		""" Leave a room """
		room = self.foreground_room
		roomhandle = None
		try:
			if txt != "/part": cmd, roomhandle = txt.split(None, 1)
		except ValueError:
			print("Too many arguments")
			return True

		if room is None and not roomhandle:
			print("Not in a room, and no room specified.")
			return True

		if roomhandle: room = self.rooms.get_room(roomhandle)
		if room is None:
			print("You are not a member of that room.")
			return True

		room.leave()

		return True

	def repl_ops(self, txt):
		""" Show priviledged users """
		room = self.foreground_room
		roomhandle = None
		try:
			if txt != "/ops": cmd, roomhandle = txt.split(None, 1)
		except ValueError:
			print("Too many arguments")
			return True

		if room is None and not roomhandle:
			print("Not in a room, and no room specified.")
			return True

		if roomhandle: room = self.rooms.get_room(roomhandle)
		if room is None:
			print("You are not a member of that room.")
			return True

		ops = self.sdkclient.api.get_power_levels(room.room_id)
		pprint.pprint(ops)

		return True

	def repl_aliases(self, txt):
		""" Show a room's known aliases """
		room = self.foreground_room
		roomhandle = None
		try:
			if txt != "/aliases": cmd, roomhandle = txt.split(None, 1)
		except ValueError:
			print("Too many arguments")
			return True

		if room is None and not roomhandle:
			print("Not in a room, and no room specified.")
			return True

		if roomhandle: room = self.rooms.get_room(roomhandle)
		if room is None:
			print("You are not a member of that room.")
			return True

		print("Room ID:", repr(room.room_id))
		print("Canonical alias:", repr(room.canonical_alias))
		print("Other aliases:", repr(room.aliases))

		return True

	def repl_restrict(self, txt):
		""" Show messages from the current room only (on/off) """

		arg = ""
		s = txt.split(None, 1)
		cmd = s[0]
		if len(s) > 1: arg = s[1].strip().lower()

		print(repr(arg))

		if arg in ("0", "off", "false"):
			self.restrict_to_foreground = False
		elif arg in ("", "1", "on", "true"):
			self.restrict_to_foreground = True
			if not self.foreground_room:
				print("Warning: No room is currently active. No messages will be shown. (See /open, /join).")
		else:
			print("Invalid argument. Please specify 'on' or 'off'.")

		return True

	def repl_quit(self, txt):
		""" Quit the program """
		return False

	def repl_help(self, txt):
		""" Show this help text """
		cmds = tuple(filter(lambda x: x.startswith('repl_'), dir(self)))
		maxlen = max(map(lambda x: len(x), cmds))
		fmt = "/{{:<{}}}".format(maxlen - 4)
		for mname in cmds:
			cmd = mname[5:]
			m = getattr(self, mname)
			print(fmt.format(cmd + ":"), getattr(m, '__doc__', "").strip())

		return True

	def repl(self, exception_handler=True):
		# Read Eval Print Loop

		# A simple console client loop that can be used as a basis for a client
		# or as a bot manhole.

		if exception_handler is True:
			exception_handler = self.on_exception

		while True:
			try:
				if not self._repl_inner(): break
			except Exception as e:
				if exception_handler is None: raise
				exception_handler(e)

	def _repl_inner(self):
		""" Run a single REPL iteration.

		Returns True if there should be another iteration, False if it
		wants to exit normally. """

		try:
			txt = input()
		except EOFError:
			return False

		if txt.startswith('/'):
			if txt.startswith('//'):
				txt = txt[1:]
			else:
				cmd = txt.split(None, 1)[0].lstrip('/')
				m = getattr(self, 'repl_' + cmd, None)
				if callable(m):
					if not m(txt): return False
				else:
					print("Unrecognized command: {!r}. Try /help.".format(cmd))
				return True

		if not self.foreground_room:
			print("Cannot send message: You have not selected any room. Try /help.")
			return True

		send_as_notice = getattr(self, 'is_bot', False)
		if txt.startswith(' '):
			txt = txt[1:]
			send_as_notice = not send_as_notice

		if send_as_notice:
			self.foreground_room.send_notice(txt)
		else:
			self.foreground_room.send_text(txt)

		return True

